#include "trajectory_generation.h"

Polynomial::Polynomial(){};

Polynomial::Polynomial(const double &piIn, const double &pfIn, const double & DtIn){
};

void          Polynomial::update(const double &piIn, const double &pfIn, const double & DtIn){
};

const double  Polynomial::p     (const double &t){
  return 0;
};

const double  Polynomial::dp    (const double &t){
  return 0;
};

Point2Point::Point2Point(const Eigen::Affine3d & X_i, const Eigen::Affine3d & X_f, const double & DtIn){
}

Eigen::Affine3d Point2Point::X(const double & time){
  return Eigen::Affine3d::Identity();
}

Eigen::Vector6d Point2Point::dX(const double & time){
  return Eigen::Vector6d::Zero();
}