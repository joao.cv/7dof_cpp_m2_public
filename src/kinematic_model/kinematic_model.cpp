#include "kinematic_model.h"
#include <iostream>
RobotModel::RobotModel(const std::array<double,8> &aIn, const std::array<double,8> &dIn, const std::array<double,8> &alphaIn):
  a     {aIn},
  d     {dIn},
  alpha {alphaIn}
{ 
};

void RobotModel::FwdKin(Eigen::Affine3d &xOut, Eigen::Matrix67d &JOut, const Eigen::Vector7d & qIn){
}