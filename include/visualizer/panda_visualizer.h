#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "eigen3/Eigen/Core"
#include <vector>
#include <atomic>
#include <thread>

extern "C" {
    #include "extApi.h"
}


namespace pandev{
  class visualizer{
    public:
      visualizer(const bool & stop_by_the_endIn = true, const bool & already_runningIn = false);
      ~visualizer();
      void setPositions(const Eigen::VectorXd &);
    private:
      int         simstart_result     {0};
      std::thread t1;
      const bool stop_by_the_end;
      const bool already_running;
      int _laser_handle;      
      int _port;
      int _waitUntilConnected;
      int _doNotReconnectOnceDisconnected;
      int _timeOutInMs;
      int _commThreadCycleInMs;
      int _ftHandle;
      int _client_id;
      const double _laser_offset;
      std::vector<int> _jointHandle;
      std::vector<double> _q_vector;  
      size_t index;
  };
}